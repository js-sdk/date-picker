import postcss from 'rollup-plugin-postcss'
import autoprefixer from 'autoprefixer'
import terser from '@rollup/plugin-terser'
import commonjs from '@rollup/plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve';

const ENV_PROD = process.env.BUILD === 'production';

export default [{
  external: [],
  input: 'src/js/plugin.js',
  output: {
    file: `dist/plugin.js`,
    format: 'es',
    sourcemap: false,
    globals: {},
    plugins: [
      ENV_PROD && terser({
        output: {
          comments: () => {},
        },
      }),
    ]
  },
  plugins: [
    commonjs({
      sourceMap: false
    }),
    resolve(),
  ]
}, {
  external: ['jquery', './plugin'],
  input: 'src/js/jquery.plugin.js',
  output: {
    file: `dist/jquery.plugin.js`,
    format: 'es',
    sourcemap: false,
    globals: {},
    plugins: [
      ENV_PROD && terser({
        output: {
          comments: () => {},
        },
      }),
    ]
  },
  plugins: [
    commonjs({
      sourceMap: false
    }),
    resolve(),
    postcss({
      extract: 'index.css',
      plugins: [autoprefixer],
      minimize: ENV_PROD,
    })
  ]
}]