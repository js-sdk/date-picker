import Container from "./component/container"
import EventsTrait from "./support/events-trait";
import {positionToInput} from "./support/position";
import {buildOptions} from "./options";

function bootInput(input) {
  input.setAttribute('readonly', true)
  input.addEventListener('focus', () => {
    setTimeout(() => { this.show() }, 10)
  })

  this.selection.on('select', function () {
    input.value = this.selection.toString(this.options)
  }, this)
}

function onDocumentClick(e) {
  const $input = this.input
  if ($input && $input === e.target) {
  } else if (!this.el.contains(e.target) && this.el !== e.target)
    this.hide()
}

function onSelect() {
  this.trigger('select')

  if (this.option('autoApply'))
    this.hide()
}

export default class Plugin {
  #options
  #container

  constructor(options) {
    options = buildOptions(options)

    this.bindOptions(options, ['ready', 'select'])

    this.#options = options
    this.#container = new Container(options)
    this.selection.on('select', onSelect, this)

    if (options.inline)
      this.show()

    if (options.input)
      bootInput.bind(this)(options.input)

    this.trigger('ready')
  }

  get options() { return this.#options }

  get container() { return this.#container }

  get calendar() { return this.#container.calendar }

  get selection() { return this.calendar.selection }

  get input() { return this.#options.input }

  get el() { return this.#container.el }

  option(key, def) { return this.#options[key] || def }

  show() {
    if (!this.#container.isHidden())
      return

    const input = this.#options.input
    if (input && input.value)
      this.selection.fromString(input.value, this.options)
    else
      this.selection.reset()

    this.#container.show()

    if (!this.#options.inline) {
      positionToInput.call(this, this.#options.input, this.#container.el)
      document.addEventListener('click', this.__hideListener = onDocumentClick.bind(this))
    }
  }

  hide() {
    this.#container.hide()

    if (!this.#options.inline) {
      document.removeEventListener('click', this.__hideListener)
      this.__hideListener = undefined
    }
  }
}

Object.assign(Plugin.prototype, EventsTrait)