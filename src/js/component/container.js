import Calendar from "./calendar"
import {selectionFactory} from "./selection"
import RangeMenu from "./range-menu"
import {presetsFactory} from "../support/presets"
import {createElement} from "../support/dom.js";
import {disabledRangesFactory} from "./disabled-ranges.js";

export default class Container {
  #options
  #el
  #calendar

  constructor(options) {
    this.#options = options
    this.#calendar = new Calendar(
      selectionFactory(options.mode),
      disabledRangesFactory(options),
      options
    )
  }

  get calendar() { return this.#calendar }

  get el() {
    if (this.#el)
      return this.#el

    let cls = 'dp-container-wrapper'
    if (!this.#options.inline)
      cls += ' dp-popup'

    const $el = createElement('<div class="' + cls + '"><div class="dp-container"></div></div>')
    const $wrap = $el.firstChild

    if (this.#options.presets) {
      const rangeMenu = new RangeMenu(this.#calendar.selection, presetsFactory(this.#options.mode))
      $wrap.appendChild(rangeMenu.el)
    }

    $wrap.appendChild(this.#calendar.el)

    if (this.#options.renderTo)
      this.#options.renderTo.appendChild($el)

    return this.#el = $el
  }

  isRendered() { return !!this.#el }

  isHidden() { return !this.#el || this.#el.style.display === 'none' }

  show() {
    this.el.style.display = ''
  }

  hide() {
    this.#el.style.display = 'none'
  }
}
