import {createFromFormat, dateFormat, dateISOString} from "../support/date"
import EventsTrait from "../support/events-trait"

function rangeFromString(value, options) {
  if (!value)
    return null

  const a = value.split(options.delimiter)
  if (a.length !== 2)
    return null

  const start = createFromFormat(options.format, a[0])
  const end = createFromFormat(options.format, a[1])
  if (start && end)
    return [start, end]
  else
    return null
}

export function selectionFactory(mode) {
  if (mode === 'range')
    return new DatePeriodSelection()
  else
    return new SingleDateSelection()
}

export class DatePeriodSelection {
  #start
  #end

  get leftBound() { return this.#start }

  get rightBound() { return this.#end }

  isSelected(date) {
    const d = dateISOString(date)

    return d >= dateISOString(this.#start) && d <= dateISOString(this.#end)
  }

  testRangeMatches(from, to) {
    return this.#start && this.#end
      && dateISOString(from) === dateISOString(this.#start)
      && dateISOString(to) === dateISOString(this.#end)
  }

  setRange(dateFrom, dateTo) {
    this.#start = dateFrom
    this.#end = dateTo

    this.trigger('change')
    this.trigger('select')
  }

  setPreset(value) {
    const a = value.split(':')
    this.setRange(new Date(a[0]), new Date(a[1]))
  }

  select(date) {
    if (this.#end) {
      this.#start = date
      this.#end = null
    } else if (this.#start) {
      if (this.#start.getTime() < date.getTime())
        this.#end = date
      else {
        this.#end = this.#start
        this.#start = date
      }
    } else
      this.#start = date

    this.trigger('change')
    if (this.#start && this.#end)
      this.trigger('select')
  }

  reset() {
    this.#start = null
    this.#end = null
    this.trigger('change')
  }

  toString(options) {
    if (this.#start && this.#end)
      return dateFormat(this.#start, options.format)
        + options.delimiter
        + dateFormat(this.#end, options.format)
    else
      return '';
  }

  fromString(value, options) {
    const r = rangeFromString(value, options)
    if (r)
      this.setRange(r[0], r[1])
    else
      this.reset()
  }
}

Object.assign(DatePeriodSelection.prototype, EventsTrait)

export class SingleDateSelection {
  #date

  get leftBound() { return this.#date }

  get rightBound() { return this.#date }

  isSelected(date) {
    return this.#date && dateISOString(date) === dateISOString(this.#date)
  }

  testRangeMatches(from, to) {
    return this.#date
      && dateISOString(from) === dateISOString(this.#date)
      && dateISOString(to) === dateISOString(this.#date)
  }

  setDate(date) {
    this.#date = date
    this.trigger('change')
    this.trigger('select')
  }

  setPreset(value) { this.setDate(new Date(value)) }

  select(date) {
    if (this.isSelected(date))
      this.reset()
    else
      this.setDate(date)
  }

  reset() {
    this.#date = null
    this.trigger('change')
  }

  toString(options) {
    return this.#date ? dateFormat(this.#date, options.format) : ''
  }

  fromString(value, options) {
    const d = createFromFormat(options.format, value)
    if (d)
      this.setDate(d)
    else
      this.reset()
  }
}

Object.assign(SingleDateSelection.prototype, EventsTrait)
