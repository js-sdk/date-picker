import {createFromFormat, dateFactory, dateFormat, dateISOString} from "../support/date"

class Range {
  dateIncluded(date) {

  }
}

export function disabledRangesFactory(options) {
  const model = new DisabledRanges()

  if (options.maxDate)
    model.setMaxDate(options.maxDate)

  if (options.minDate)
    model.setMinDate(options.minDate)

  // if (options.disabled)
  //   options.disabled.forEach(d => {
  //     model.addDate(d)
  //   })

  return model
}

export class DisabledRanges {
  #rages = []
  #minDate
  #maxDate

  isDateDisabled(date) {
    if (typeof date === 'string')
      date = new Date(date)
    return (this.#maxDate && date.getTime() >= this.#maxDate.getTime())
      || (this.#minDate && date.getTime() <= this.#minDate.getTime())
      || !!this.#rages.find(r => r.dateIncluded(date))
  }

  addRange(dateFrom, dateTo) {

  }

  addDate() {}

  setMaxDate(date) {
    this.#maxDate = dateFactory(date)
    this.#maxDate.setHours(23, 59, 59)
  }

  setMinDate(date) {
    this.#minDate = dateFactory(date)
    this.#minDate.setHours(0, 0, 0)
  }

  reset() {
    this.#rages = []
  }
}
