import {createElement} from "../support/dom.js";

export default class RangeMenu {
  #el
  #items
  #selection

  constructor(selection, items) {
    this.#selection = selection
    this.#items = items

    selection.on('change', this.updateSelection, this)
  }

  get el() {
    if (this.#el)
      return this.#el

    let html = '<div class="dp-ranges-menu">'
    this.#items.forEach(item => {
      html += '<div class="range" data-value="' + item.value + '">' + item.text + '</div>'
    })
    html += '</div>';

    const $el = createElement(html)
    const selection = this.#selection
    const onClick = function () {
      selection.setPreset(this.getAttribute('data-value'))
    }
    $el.childNodes.forEach($item => {
      $item.addEventListener('click', onClick);
    })

    this.#el = $el

    this.updateSelection()

    return $el
  }

  updateSelection() {
    const $el = this.#el
    const selection = this.#selection

    $el.childNodes.forEach($item => {
      if (selection.testRangeMatches(new Date($item.getAttribute('data-from')), new Date($item.getAttribute('data-to'))))
        $item.classList.add('selected')
      else
        $item.classList.remove('selected')
    })
  }
}
