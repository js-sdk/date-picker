import {dateISOString, datesInRange} from "../support/date"
import {bootHead, buildDaysHtml} from "../support/html-builder"
import {Navigation} from "./navigation"
import {createElement} from "../support/dom.js";

function updateSelection(start, end) {
  const $wrap = this.el.querySelector('div.calendars')

  $wrap.querySelectorAll('div.days-grid div.selected').forEach($item => {
    $item.classList.remove('selected', 'first', 'last', 'in-range')
  })

  const dates = datesInRange(start, end)
  const l = dates.length - 1
  dates.forEach((d, i) => {
    const $day = $wrap.querySelector('div[data-date="' + dateISOString(d) + '"]')
    if (!$day)
      return true

    let cls = ['selected']
    if (i > 0 && i < l)
      cls[cls.length] = 'in-range'
    else {
      if (i === 0)
        cls[cls.length] = 'first'
      if (i === l)
        cls[cls.length] = 'last'
    }

    $day.classList.add(...cls)
  })
}

function updateBounds() {
  const selection = this.selection
  const left = selection.leftBound
  const right = selection.rightBound
  if (!(left && right) || (this.dateInRange(left) && this.dateInRange(right)))
    return

  setTimeout(() => {
    this.setMonth(left.getMonth(), left.getFullYear())
  }, 10)
}

function startNavigation() {
  const nav = new Navigation(this.monthDate, this.options)
  this.el.append(nav.el)
  nav.on('select', (month, year) => {
    nav.destroy()
    this.setMonth(month, year)
  })

  return nav
}

function updateDisabled() {
  const disabledRanges = this.disabledRanges
  const $wrap = this.el.querySelector('div.calendars')

  $wrap.querySelectorAll('div.days-grid div.day').forEach($item => {
    if (disabledRanges.isDateDisabled($item.getAttribute('data-date')))
      $item.classList.add('disabled')
    else
      $item.classList.remove('disabled')
  })
}

export default class Calendar {
  #options
  #el
  #monthDate
  #selection
  #navigation
  #disabledRanges

  constructor(selection, disabledRanges, options) {
    this.#options = Object.assign({
      calendars: 1
    }, options)
    this.#monthDate = new Date()
    this.#monthDate.setDate(1)
    this.#selection = selection
    this.#disabledRanges = disabledRanges
  }

  get el() {
    if (this.#el)
      return this.#el

    this.#el = createElement('<div class="dp-calendar-wrapper"></div>')
    this.update()

    this.#selection
      .on('change', this.updateSelection, this)
      .on('select', updateBounds, this)

    return this.#el
  }

  get options() { return this.#options }

  get monthDate() { return this.#monthDate }

  get selection() { return this.#selection }

  get disabledRanges() { return this.#disabledRanges }

  update() {
    if (this.#navigation) {
      this.#navigation.destroy()
      this.#navigation = undefined
    }

    const $el = this.#el
    const self = this
    const monthDate = this.#monthDate
    const disabledRanges = this.disabledRanges

    $el.innerHTML = '<div class="calendars">'
      + buildDaysHtml(monthDate, this.#options.calendars)
      + '</div>'

    const clickListener = function () {
      if (this.classList.contains('disabled'))
        return
      self.#selection.select(new Date(this.getAttribute('data-date')))
    }
    const hoverListener = function () {
      if (this.classList.contains('disabled'))
        return
      const selection = self.#selection
      const left = selection.leftBound
      const right = selection.rightBound
      if (left && !right)
        updateSelection.call(self, left, new Date(this.getAttribute('data-date')))
    }

    const $wrap = $el.firstChild
    $wrap.querySelectorAll('div.day').forEach($item => {
      // if (disabledRanges.isDateDisabled($item.getAttribute('data-date')))
      //   return
      $item.addEventListener('click', clickListener)
      $item.addEventListener('mouseover', hoverListener)
    })

    bootHead($wrap, {
      title: () => { this.#navigation = startNavigation.call(this) },
      prev: () => { this.setMonth(monthDate.getMonth() - 1) },
      next: () => { this.setMonth(monthDate.getMonth() + 1) }
    })

    this.updateSelection()

    updateDisabled.call(this)
  }

  setMonth(month, year) {
    if (this.#monthDate.getMonth() === month && (undefined === year || this.#monthDate.getFullYear() === year))
      return

    this.#monthDate.setMonth(month)
    if (year)
      this.#monthDate.setFullYear(year)

    this.update()
  }

  getMonth() { return this.#monthDate.getMonth() }

  getFullYear() { return this.#monthDate.getFullYear() }

  dateInRange(date) {
    const d = dateISOString(date)
    const $days = this.#el.querySelectorAll('div.day')
    const last = $days[$days.length - 1].getAttribute('data-date')

    return d >= dateISOString(this.#monthDate) && d <= last
  }

  updateSelection() {
    const selection = this.#selection
    updateSelection.call(this, selection.leftBound, selection.rightBound)
  }

  destroy() {
    this.#selection.unbind('change', this.updateSelection)
    this.#selection = undefined
    if (this.#el) {
      this.#el.remove()
      this.#el = undefined
    }
    if (this.#navigation) {
      this.#navigation.destroy()
      this.#navigation = undefined
    }
    this.#monthDate = undefined
  }
}
