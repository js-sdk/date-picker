import EventsTrait from "../support/events-trait"
import {bootGrid, bootHead, buildMonthsHtml, buildYearsHtml} from "../support/html-builder";
import {createElement} from "../support/dom.js";

const MODE_MONTHS = 'months'
const MODE_YEARS = 'years'

function renderMonths() {
  const $el = this.el

  $el.innerHTML = buildMonthsHtml(this.year, this.calendars)

  bootHead($el, {
    title: () => { this.setMode(MODE_YEARS) },
    prev: () => { this.setYear(this.year - 1) },
    next: () => { this.setYear(this.year + 1) }
  })

  bootGrid($el, 'div.grid div.unit', ($item) => {
    this.trigger('select', $item.getAttribute('data-month'), $item.getAttribute('data-year'))
  })
}

function renderYears() {
  const $el = this.el

  $el.innerHTML = buildYearsHtml(this.year, this.calendars)

  bootHead($el, {
    title: () => { },
    prev: () => { this.setYear(this.year - 10) },
    next: () => { this.setYear(this.year + 10) }
  })

  bootGrid($el, 'div.grid div.unit', ($item) => {
    this.setYear($item.getAttribute('data-year'), false)
    this.setMode(MODE_MONTHS)
  })
}

export class Navigation {
  #el
  #year
  #mode = MODE_MONTHS

  constructor(monthDate, options) {
    this.#year = monthDate.getFullYear()
    this.calendars = options.calendars || 1
  }

  get year() { return this.#year }

  setMode(mode) {
    this.#mode = mode
    this.update()
  }

  setYear(year, flag) {
    this.#year = year
    if (false !== flag)
      this.update()
  }

  get el() {
    if (this.#el)
      return this.#el

    this.#el = createElement('<div class="dp-navigation-wrapper"></div>');

    this.update()

    return this.#el
  }

  update() {
    if (this.#mode === MODE_MONTHS)
      renderMonths.call(this)
    else
      renderYears.call(this)
  }

  destroy() {
    if (this.#el) {
      this.#el.remove()
      this.#el = undefined
    }
    this.#year = undefined
    this.#mode = undefined

    this.unbind()
  }
}

Object.assign(Navigation.prototype, EventsTrait)
