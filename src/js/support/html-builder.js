import {dayNamesShort, monthNames, monthNamesShort} from "./util";
import {dateISOString} from "./date";

const todayDateString = dateISOString(new Date())

function buildHead(title, i, count) {
  let html = '<div class="head">'
  if (i === 0)
    html += '<div class="unit prev"></div>'
  else
    html += '<div class="empty"></div>'
  html += '<div class="unit title">' + title + '</div>'
  if (i === count - 1)
    html += '<div class="unit next"></div>'
  else
    html += '<div class="empty"></div>'
  html += '</div>'

  return html
}

export function buildDaysHtml(startDate, count) {
  const startMonth = startDate.getMonth()
  let html = ''

  for (let i = 0; i < count; i++) {
    const date = new Date(startDate.getTime())
    if (i > 0)
      date.setMonth(startMonth + i)
    const M = date.getMonth()
    const Y = date.getFullYear()
    date.setDate(date.getDay() === 0 ? -5 : 2 - date.getDay())

    html += '<div class="dp-calendar">'
    html += buildHead(monthNames[M] + ' ' + Y, i, count)
    html += '<div class="daynames">'
    for (let day = 0; day < 7; day++) {
      html += '<div>' + dayNamesShort[day] + '</div>'
    }
    html += '</div>'

    html += '<div class="days-grid">'
    for (let week = 0; week < 6; week++) {
      for (let day = 0; day < 7; day++) {
        if (date.getMonth() !== M)
          html += '<div class="empty"></div>'
        else {
          const dateString = dateISOString(date)
          let cls = 'day unit'

          // if (bounds.has(dateString))
          //   cls += ' disabled';
          if (dateString === todayDateString)
            cls += ' current'
          // else if (date.getMonth() !== currentMonth)
          //   cls += ' month-alt'

          html += '<div class="' + cls + '" data-date="' + dateString + '">' + date.getDate() + '</div>'
        }

        date.setDate(date.getDate() + 1)
      }
    }
    html += '</div>'
    html += '</div>'
  }

  return html
}

export function buildMonthsHtml(startYear, count) {
  const now = new Date()
  const currentYM = now.getFullYear() + '-' + now.getMonth()
  let html = ''
  let year = startYear
  for (let i = 0; i < count; i++) {
    html += '<div class="dp-wrapper">'
    html += buildHead(year, i, count)
    html += '<div class="grid">'
    monthNamesShort.forEach((n, i) => {
      let cls = 'unit'
      if (currentYM === year + '-' + i)
        cls += ' current'
      html += '<div class="' + cls + '" data-year="' + year + '" data-month="' + i + '">' + n + '</div>'
    })
    html += '</div>'
    html += '</div>'
    year++
  }

  return html
}

export function buildYearsHtml(startYear, count) {
  const Y = (new Date()).getFullYear()
  let html = ''
  let year = startYear - startYear % 10
  for (let i = 0; i < count; i++) {
    html += '<div class="dp-wrapper">'
    html += buildHead(year + '-' + (year + 9), i, count)
    html += '<div class="grid">'
    html += '<div class="empty"></div>'
    for (let i = 0; i < 10; i++) {
      let cls = 'unit'
      if (Y === year)
        cls += ' current'
      html += '<div class="' + cls + '" data-year="' + year + '">' + year + '</div>'
      year++
    }
    html += '<div class="empty"></div>'
    html += '</div>'
    html += '</div>'
  }

  return html
}

export function bootHead($el, handlers) {
  $el.querySelectorAll('div.head').forEach($head => {
    const $prev = $head.querySelector('div.prev')
    const $next = $head.querySelector('div.next')

    $head.querySelector('div.title').addEventListener('click', () => {
      setTimeout(handlers.title, 10)
    });

    if ($prev)
      $prev.addEventListener('click', () => { setTimeout(handlers.prev, 10) })

    if ($next)
      $next.addEventListener('click', () => { setTimeout(handlers.next, 10) })
  })
}

export function bootGrid($el, selector, clickHandler) {
  const listener = function () {
    setTimeout(() => { clickHandler(this) }, 10)
  }

  $el.querySelectorAll(selector).forEach($item => {
    $item.addEventListener('click', listener)
  })
}