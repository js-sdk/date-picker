import {css} from "./dom.js";

export function positionToInput($input, $el) {
  const W = window.innerWidth
  const H = window.innerHeight + window.scrollLeft
  css($el, {
    visibility: 'hidden',
    top: 0,
    left: 0
  })
  const offset = $input.getBoundingClientRect();
  let left = offset.left
  let top = offset.top + $input.offsetHeight + 4
  const w = $el.offsetWidth
  const h = $el.offsetHeight
  if (left + w > W)
    left = offset.left + $input.offsetWidth - w

  if (top + h > H)
    top = offset.top - h - 4

  css($el, {
    visibility: 'visible',
    top: `${top}px`,
    left: `${left}px`
  })
}