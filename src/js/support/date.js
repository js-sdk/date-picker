function pad(v) {
  return (v > 9 ? '' : '0') + v
}

const dateFactories = {
  now: () => new Date(),
  yesterday: () => {
    const d = new Date()
    d.setDate(d.getDate() - 1)
    return d
  },
  tomorrow: () => {
    const d = new Date()
    d.setDate(d.getDate() + 1)
    return d
  }
}

export function dateFactory(date) {
  if (date instanceof Date)
    return new Date(date.getTime())

  if (typeof date !== 'string') {
    return new Date(date)
  }

  if (dateFactories[date])
    return dateFactories[date]()

  switch (date) {
    case 'first day of this month':
      return (() => {
        const d = new Date()
        d.setDate(1)

        return d
      })()
    case 'last day of this month':
      return (() => {
        const d = new Date()
        d.setMonth(d.getMonth() + 1, 0)

        return d
      })()
    case 'first day of this year':
      return (() => {
        const d = new Date()
        d.setMonth(0, 1)

        return d
      })()
    case 'last day of this year':
      return (() => {
        const d = new Date()
        d.setMonth(11, 31)

        return d
      })()
    default:
      return new Date(date)
  }
}

export function dateISOString(date) {
  const mm = date.getMonth() + 1; // getMonth() is zero-based
  const dd = date.getDate();
  return [date.getFullYear(), pad(mm), pad(dd)].join('-');
}

export function dateFormat(date, format) {
  const mm = date.getMonth() + 1; // getMonth() is zero-based
  const dd = date.getDate();

  return format
    .replace('YYYY', date.getFullYear())
    .replace('MM', pad(mm))
    .replace('DD', pad(dd));
}

export function createFromFormat(format, value) {
  const r = format
    .replace('DD', '(?<d>\\d{2})')
    .replace('MM', '(?<m>\\d{2})')
    .replace('YYYY', '(?<Y>\\d{4})')
    .replaceAll('.', '\\.')
  const m = value.match(new RegExp('^' + r + '$', ''))
  if (!m)
    return null

  return new Date(`${m.groups.Y}-${m.groups.m}-${m.groups.d}`)
}

export function datesInRange(dateFrom, dateTo) {
  if (dateFrom && dateTo) {
    if (dateFrom.getTime() > dateTo.getTime()) {
      const d = dateFrom
      dateFrom = dateTo
      dateTo = d
    }

    let date = new Date(dateFrom.getTime())
    let dates = [date]

    const de = dateISOString(dateTo)
    do {
      const ds = dateISOString(date)
      if (ds === de)
        break
      date = new Date(date.getTime())
      date.setDate(date.getDate() + 1)
      dates[dates.length] = date
    } while (true)

    return dates;
  } else if (dateFrom)
    return [dateFrom]
  else if (dateTo)
    return [dateTo]
  else
    return []
}
