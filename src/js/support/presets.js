import {dateFactory, dateISOString} from "./date";
import {__} from "./locale";
import {periodFactory} from "../model/period";

function makePeriodPreset(text, period) {
  return {
    text: text,
    value: dateISOString(period.from) + ':' + dateISOString(period.to)
  }
}

function makeDatePreset(text, date) {
  return {
    text: text,
    value: dateISOString(date)
  }
}

export function presetsFactory(mode) {
  if (mode === 'range')
    return periodPresets
  else
    return datePresets
}

export const periodPresets = [
  makePeriodPreset(__('today'), periodFactory('today')),
  makePeriodPreset(__('thismonth'), periodFactory('thismonth')),
  makePeriodPreset(__('thisyear'), periodFactory('thisyear')),
  makePeriodPreset(__('last7days'), periodFactory('last7days')),
  makePeriodPreset(__('last30days'), periodFactory('last30days')),
  makePeriodPreset(__('prevweek'), periodFactory('prevweek')),
  makePeriodPreset(__('prevmonth'), periodFactory('prevmonth')),
  makePeriodPreset(__('prevyear'), periodFactory('prevyear')),
]

export const datePresets = [
  makeDatePreset(__('today'), dateFactory('now')),
  makeDatePreset(__('yesterday'), dateFactory('yesterday')),
  makeDatePreset(__('tomorrow'), dateFactory('tomorrow')),
  makeDatePreset(__('first day of month'), dateFactory('first day of this month')),
  makeDatePreset(__('last day of month'), dateFactory('last day of this month')),
  makeDatePreset(__('first day of year'), dateFactory('first day of this year')),
  makeDatePreset(__('last day of year'), dateFactory('last day of this year')),
]
