export function createElement(html) {
  const template = document.createElement('template');
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild;
}

export function css($el, styles) {
  for (let i in styles) {
    $el.style[i] = styles[i]
  }
}