export default {
  today: 'Сегодня',
  prevweek: 'Прошлая неделя',
  thismonth: 'Текущий месяц',
  prevmonth: 'Прошлый месяц',
  thisyear: 'Текущий год',
  prevyear: 'Прошлый год',
  last7days: 'Последние 7 дней',
  last30days: 'Последние 30 дней',
  yesterday: 'Вчера',
  tomorrow: 'Завтра',
  'first day of month': 'Начало месяца',
  'last day of month': 'Конец  месяца',
  'first day of year': 'Начало года',
  'last day of year': 'Конец года'
}