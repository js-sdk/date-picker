const periodFactories = {
  today: () => new Period(new Date(), new Date()),
  prevweek: () => {
    const dateFrom = new Date()
    const dateTo = new Date()

    dateFrom.setDate(dateFrom.getDate() - dateFrom.getDay() + 1 - 7);
    dateTo.setMonth(dateFrom.getMonth(), dateFrom.getDate() + 6);

    return new Period(dateFrom, dateTo)
  },
  thismonth: () => {
    const dateFrom = new Date()
    const dateTo = new Date()

    dateFrom.setDate(1)
    dateTo.setMonth(dateTo.getMonth() + 1, 0)

    return new Period(dateFrom, dateTo)
  },
  prevmonth: () => {
    const dateFrom = new Date()
    const dateTo = new Date()

    dateFrom.setMonth(dateFrom.getMonth() - 1, 1)
    dateTo.setDate(0)

    return new Period(dateFrom, dateTo)
  },
  thisyear: () => {
    const dateFrom = new Date()
    const dateTo = new Date()
    dateFrom.setMonth(0, 1)
    dateTo.setMonth(11, 31)

    return new Period(dateFrom, dateTo)
  },
  prevyear: () => {
    const dateFrom = new Date()
    dateFrom.setFullYear(dateFrom.getFullYear() - 1, 0, 1)
    const dateTo = new Date(dateFrom.getTime())
    dateTo.setMonth(11, 31)

    return new Period(dateFrom, dateTo)
  },
  last7days: () => {
    const dateFrom = new Date()
    const dateTo = new Date()
    dateFrom.setDate(dateFrom.getDate() - 6)

    return new Period(dateFrom, dateTo)
  },
  last30days: () => {
    const dateFrom = new Date()
    const dateTo = new Date()
    dateFrom.setDate(dateFrom.getDate() - 29)

    return new Period(dateFrom, dateTo)
  }
}

export class Period {
  constructor(from, to) {
    this.from = from
    this.to = to
  }
}

export function periodFactory(value) {
  return periodFactories[value]()
}