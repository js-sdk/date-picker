const defaultOptions = {
  mode: 'date',
  locale: 'ru',
  inline: false,
  calendars: 1,
  delimiter: ' - ',
  format: "DD.MM.YYYY",
  autoApply: false
}

function buildModeOptions(mode) {
  if (mode === 'range')
    return {
      calendars: 2,
    }
  else
    return {}
}

export function buildOptions(options) {
  options = Object.assign({}, defaultOptions, buildModeOptions(options.mode || 'date'), options)

  if (options.inline)
    options.autoApply = false
  else
    options.autoApply ??= true

  if (options.renderTo && typeof options.renderTo === 'string')
    options.renderTo = document.querySelector(options.renderTo)

  if (options.input) {
    options.input = typeof options.input === 'string' ? document.querySelector(options.input) : options.input
    options.renderTo ??= options.input.parentNode
  }

  return options
}