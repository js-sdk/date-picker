import $ from 'jquery'
import Plugin from "./plugin"

import "../sass/main.scss"

$.fn.dateRangePicker = function (options) {
  return $(this).each(function () {
    new Plugin(Object.assign({
      input: this,
      mode: 'range',
      calendars: 2
    }, options))
  })
}

$.fn.datePicker = function (options) {
  return $(this).each(function () {
    new Plugin(Object.assign({
      input: this,
      mode: 'date',
      calendars: 1
    }, options))
  })
}